# How to run

1. Create mongoDB database 'reigntest' with collections 'articles' and 'added_articles'.
2. node server.js.
3. Run the url http://localhost:3000/save_articles.
4. ng serve inside reignclient.