import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'reignclient';
  articles = [];

  constructor(private http: HttpClient)
  {
      this.http = http;
  }

  ngOnInit()
  {
      this.http.get('http://localhost:3000/articles',{responseType: 'json'})
      .subscribe(data => {
          console.log(data);
          //real_data : Any[] = data;
          //console.log(real_data);
          this.articles = eval(JSON.stringify(data));
      });
  }

    remove_article(object_id)
    {
        this.http.get('http://localhost:3000/remove_article?object_id='+object_id,{responseType: 'json'})
        .subscribe(data => {
        });
        $('#article_'+object_id).remove();
    }
}
