//Load HTTP module
const http = require("http");
const url = require('url');
const hostname = '127.0.0.1';
const port = 3000;

//Create HTTP server and listen on port 3000 for requests
const server = http.createServer((req, res) => {
    const queryObject = url.parse(req.url,true);
    if (req.url == "/save_articles")
    {
        save_articles();
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.end('Hello World\n');
    }
    else if (req.url == "/articles")
    {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader('Content-Type', 'application/json');

        var MongoClient = require('mongodb').MongoClient;
        var url2 = "mongodb://localhost:27017/";

        MongoClient.connect(url2, function(err, db) {
            if (err) throw err;
            var dbo = db.db("reigntest");
            dbo.collection("articles").find({}).toArray(function (err2, res2){
                res.end(JSON.stringify(res2));
                db.close();
            });
        });
    }
    else if (queryObject.pathname == "/remove_article")
    {
        var MongoClient = require('mongodb').MongoClient;
        var url2 = "mongodb://localhost:27017/";

        MongoClient.connect(url2, function(err, db) {
            var dbo = db.db("reigntest");
            dbo.collection("articles").deleteOne({
                objectID: queryObject.query.object_id
            }, function(err, obj) {
                if (err) throw err;
                db.close();
            });
        });
    }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

function save_articles()
{
    const fetch = require('node-fetch');
    fetch('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .then(response => response.json())
        .then(data => {
            var MongoClient = require('mongodb').MongoClient;
            var url2 = "mongodb://localhost:27017/";

            MongoClient.connect(url2, function(err, db) {
              if (err) throw err;
              var dbo = db.db("reigntest");
              for (var i = 0; i < data['hits'].length; i++)
              {
                  dbo.collection("added_articles").findOne({
                      objectID: data['hits'][i]["objectID"]
                  },function(err, res){
                      if(res == null && data['hits'][i] !== undefined)
                      {
                          dbo.collection("articles").insertOne(data['hits'][i]);
                          dbo.collection("added_articles").insertOne({
                              objectID: data['hits'][i]["objectID"]
                          });
                      }
                  });
                }
              db.close();
            });
        });
        setTimeout(function(){ save_articles(); },1000*60*60);
}
